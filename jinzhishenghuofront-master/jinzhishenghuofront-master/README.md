版本更新说明
2018-12-02
1、修复团长从分享链接进入，显示的是其他团长信息的问题；
2、修复提交订单判断收货人手机号码，没有过滤空格的问题；

2018-12-03
1、修复首页团长购买指数没有实时更新问题；
2、修复提成统计数据计算问题；
3、带视频商品，增加关闭视频按钮（安卓系统下暂时无效）；
4、修复安卓系统下视频强制全屏播放问题（必须多点击一次才能播放暂未解决）；
5、登录页面底部增加系统版本号；

2018-12-04
1、修复提成统计商品名称没有区分规格问题；
2、修复佣金明细商品名称没有区分规格问题；
3、修复累计佣金取数错误问题；

2018-12-05
1、修复供应商中心商品没有区分规格问题；
2、修复代客下单模式，个人中心代客下单字样没有显示问题；

2018-12-06
1、余额列表增加备注；

2018-12-08
1、关闭个人中心升级vip功能；

2018-12-12
1、修复团长晒单分享图标不显示问题；
2、修复晒单中客户电话没隐藏和可修改的问题；
3、修复取消订单按钮和微信右上角菜单过近容易误操作问题；

2018-12-12_1
1、首页快捷分类图标，增加跳转到链接功能；
2、支付完成自动跳转页面，改为跳转到首页；

2018-12-12_2
1、个人中心增加ID显示；

2018-12-13
1、修复打烊团长，没有公告提示问题；
2、修复银行卡列表读取错误；
3、首页增加下单滚动播放，不是实施刷新，要切换页面才会刷新；
4、修复订单详情页面，下单所在团长可以取消客户订单问题；

2018-12-13_1
1、增加供应商中心、我的优惠券、我的银行卡、订单详情页的返回导航；

2018-12-14
1、性能原因，去掉订单广播功能；
2、商品列表增加直播图标，商品详情有直播内容的，打开后直接打开直播页面；
3、修复商品自定义属性描述过长，和分享按钮位置冲突问题；
4、代客下单的收件人姓名后面增加代客下单字样；

2018-12-14_1
1、调整首页商品分类图标，说明文字支持4个字不换行；

2018-12-14_2
1、门店订单增加返回导航；

2018-12-15
1、订单支付的倒计时计算方式调整，尝试修复部分客户订单进入支付就取消的问题；
2、增加主页背景音乐播放，播放时间限制为2018-12-16 20:00至2018-12-17 20:00；

2018-12-17
1、修复已经关闭或者打烊的团长，下属会员还能下单的问题；

2018-12-21
1、修改支付模块，增加对小程序支付的支持；
2、修改分享模块，增加对小程序分享的支持；
3、首页增加下拉刷新页面的功能；

2018-12-21_1
1、修复公众号中，安卓系统更换店铺时定位失败问题；
2、优化首页订单广播功能；