// var http = 'http://192.168.1.104:8001';

export const HOME_module = "/wxapi/home"
export const HOME_FOOTER = "/wxapi/shop/footer"
export const ALL_GOODS = "/wxapi/shop/products"
export const GOODS_STOCK = "/wxapi/product/sales"

export const ALL_CENTERS = "/wxapi/center/list"
export const NEAREST_SHOP = "/wxapi/shop/nearest"
export const SHOP_INFO = "/wxapi/shop"
export const BIND_SHOP = "/wxapi/shop/bind"
export const SHOP_BANNERS = "/wxapi/shop/banners"
export const SHOP_SUBJECT = "/wxapi/shop/subject"
export const SHOP_ICONADVERT = "/wxapi/shop/iconAdvert"
export const CART_ADD = "/wxapi/cart/add"
export const CART_EDIT = "/wxapi/cart/edit"
export const CART_DEL = "/wxapi/cart/del"
export const CART_DELALL = "/wxapi/cart/delall"
export const CART_LIST = "/wxapi/cart/list"
export const CART_TOTAL = '/wxapi/cart/total'
export const ACCESS_LOG = '/wxapi/shop/access/log'
export const SHOP_NOTICE = '/wxapi/shop/notice'
export const SHOP_PICKUP = '/wxapi/shop/pickup/list'
export const SHOP_PICKUP_GOODS = '/wxapi/shop/pickup/detail'
export const SHOP_PICKUP_CONFIRM = '/wxapi/shop/pickup/confirm'

// 小鹿集市
export const SHOP_PRODUCT_SHENXIAN = '/wxapi/shop/brandPavilion'

//运费模板
export const POST_FEE = ""
