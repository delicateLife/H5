// var http = 'http://192.168.1.104:8001';

export const WXJS_CONFIG = "/wxapi/jsconfig";
export const GLOBAL_CONFIG = "/wxapi/global/config";
export const GLOBAL_SHARE = "/wxapi/global/share";
export const UPLOAD_FILE = '/platform/config/upload';
export const QR_CREATE = '/qr/server/url';
export const ASSIST_MODE = 'assistMode';
export const FIRST_INIT = '/wxapi/firstinit';

export const ALI_API = 'https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardBinCheck=true&cardNo=';

export const VERSION = 'V20190622';
