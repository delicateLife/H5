//var http = 'http://192.168.1.104:8001';

//登录
export const AUTH_URL = "/wxapi/authurl"
export const USER_LOGIN = "/wxapi/login"
export const USER_LOGOUT = ""

//用户信息
export const USER_PROFILE = "/wxapi/member"
export const USER_MODIFY = "/wxapi/member/update"
export const USER_MODIFY_PASSWORD = ""
export const USER_CHANGE_MOBILE = ""

export const SHOP_REG = '/wxapi/shop/reg'
// 银行列表
export const CLEARN_BANK = "/wxapi/getBank"
// 行业类别
export const CLEARN_INDUSTRY = "/wxapi/getIndustry"
// 清分注册
export const CLEARN_REGISTER = "/wxapi/register"

//验证码
export const USER_SENDCODE = ""

//地址
export const ADDRESS = "/api/address"
export const ADDRESS_DEFAULT = "/api/address-default"

//收藏
export const GOODS_COLLECT_LIST = "/api/moreGoods"

export const BALANCE_PICK = "/wxapi/balance/pick"
export const BALANCE_PICK_RECORD = "/wxapi/balance/pick/record"
export const BALANCE_LIST = "/wxapi/balance/list"
export const COMMISSION_TOTAL = '/wxapi/commission/total'
export const COMMISSION_LOG = '/wxapi/commission/log'
export const COMMISSION_SUM = '/wxapi/commission/sum'
export const INTEGRAL_FLOW = '/wxapi/member/integral/flow' //积分列表

export const COMMANDER_ORDER_COUNT = "/wxapi/commander/order/count"
export const COMMANDER_INCOME_EXPECT = "/wxapi/commander/income/expect"
export const COMMANDER_MEMBER = '/wxapi/shop/members'
export const COMMANDER_TEAM = '/wxapi/commander/team'
export const COMMANDER_STATISTIC = '/wxapi/shop/statistic'
export const COMMANDER_RANKING = '/wxapi/shop/ranking'

export const WORKORDER_CATEGORY = '/wxapi/workorder/category'
export const WORKORDER_TYPE = '/wxapi/workorder/type'
export const WORKORDER_ADD = '/wxapi/workorder/add'
export const WORKORDER_DETAIL = '/wxapi/workorder/detail'
export const WORKORDER_COMMENT = '/wxapi/workorder/comment'
export const WORKORDER_CANCEL = '/wxapi/workorder/revert'

//销售统计
export const SUPPLIER_STATISTIC = '/wxapi/supply/statistic'
// 退货统计
export const RETURN_STATISTICS = '/wxapi/supply/salesReturn'

export const VIP_YUPAY = '/wxapi/vip/yupay'
export const VIP_WXPAY = '/wxapi/vip/wxpay'

export const SAVE_BANKCARD = '/wxapi/bank/addcard'
export const LIST_BANKCARD = '/wxapi/bank/listcard'

export const USER_TICKETNO = '/wxapi/user/ticketNo'

export const SHOP_APPOINT = '/wxapi/shop/appoint' //会员中心，获取单品和分类的排名信息

export const SHOP_APPOINT_PRODUCT = '/wxapi/shop/appointProduct' //会员中心，获取单品排名

export const SHOP_APPOINT_CATEGORY = '/wxapi/shop/appointCategory' //会员中心，获取分类的排名

export const APP_QRCODE = '/wxapi/app/qrcode' //在小程序环境中，获取小程序店铺码
