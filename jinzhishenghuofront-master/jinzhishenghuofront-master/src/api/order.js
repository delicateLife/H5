// var http = 'http://192.168.1.104:8001';

export const ORDER_LIST = "/wxapi/order/list"
export const WORK_LIST = "/wxapi/work/list"

export const ELE_COUPON_LIST = "/wxapi/coupon"
// 正常优惠券兑换
export const ELE_COUPON_LISTS = "/wxapi/couponList"
// 积分兑换优惠券
export const ELE_COUPON_LISTS_JIFEN = "/wxapi/integraCouponlList"
export const ELE_COUPON_CREATE = "/wxapi/createCoupon"

export const REFUND_LIST = "/api/refund-list"
export const RECEIPT_ORDER = "/wxapi/order/receipt";
export const PRE_ORDER = "/wxapi/order/precreate";
export const CREATE_ORDER = "/wxapi/order/create";
export const CANCEL_ORDER = "/wxapi/order/cancel";
export const CANCEL_GOODS = "/wxapi/order/goods/cancel";
export const ORDER_GET = "/wxapi/order";
export const ORDER_WXPAY = "/wxapi/order/wxpay";
export const ORDER_YUPAY = "/wxapi/order/yupay";
export const ORDER_COUNT = "/wxapi/order/count";
export const ORDER_COUPON = '/wxapi/order/coupon';
export const ORDER_BROADCAST = "/wxapi/order/broadcast";

//收货提醒
export const ORDER_REMIND = "/wxapi/order/goodsNotify";
//发送请求
export const ORDER_REMIND_SEND = "/wxapi/order/goodsNotifyRemind";
// 首页弹框领券
export const USER_TICKETPOP = "/wxapi/user/ticketPop";

// 领取
export const TICKETPOP_CONVERSION = "/wxapi/ticketPop/conversion";

