import '@/assets/scss/iconfont/iconfont.css';
import '@/assets/scss/iconfont/denglong/iconfont.css';
import 'vant/lib/icon/local.css';
import Vue from 'vue';
import App from './App.vue';
import router from '@/vue/router/';
import Vuelidation from '@/vue/plugins/vuelidation';
import valid from '@/vue/mixin/valid';
import common from '@/vue/mixin/common';
import VueCountdown from '@/vue/plugins/vue-countdown';
//import FastClick from 'fastclick';

//babel-polyfill
import 'babel-polyfill';
import "core-js/es6/array";
import "core-js/es6/object";
import 'core-js/es6/promise';
import 'core-js/es7/';

import {
  Lazyload,
  Toast,
  Tag,
  Dialog,
  Cell,
  CellGroup,
  Field,
  Icon,
  Button,
  Popup,
  Loading,
  List,
  Card,
} from 'vant';

import axios from '@/vue/plugins/axios';
import util from '@/assets/js/util';
import filters from "@/vue/filter/";

// plugins
Vue.use(VueCountdown);
Vue.use(axios);
Vue.use(Vuelidation);
Vue.use(valid);
Vue.use(common);

// vue
Vue.use(filters);
Vue.use(util);

import defaultImage from "@/assets/images/goods_default.jpg";
import defaultLoadImage from "@/assets/images/goods_default_load.png";
// vant
Vue.use(Lazyload, {
  preLoad: 1.3,
  error: defaultImage,
  loading: defaultLoadImage,
  attempt: 3,
  // listenEvents: ['scroll'],
  lazyComponent: true,
});
Vue.use(Tag);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Field);
Vue.use(Icon);
Vue.use(Button);
Vue.use(Popup);
Vue.use(Loading);
Vue.use(List);
Vue.use(Dialog);
Vue.use(Card);
Toast.setDefaultOptions({
  duration: 1500
});
//FastClick.attach(document.body);
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
