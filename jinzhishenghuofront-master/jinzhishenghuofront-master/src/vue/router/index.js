import Vue from 'vue';
import Router from 'vue-router';

import home from './home';
import items from './items';
import user from './user';
import order from './order';
import login from './login';

Vue.use(Router);

let RouterModel = new Router({
	mode: "history",
	base: "/wemall/",
	routes:[...home, ...items, ...user, ...order, ...login],
})
  
RouterModel.beforeEach((to, from, next) => {
	//第一次进入页面，记下地址，以便wxsdk调用config
	if (!from.name) {
		window.entryUrl = location.href.split('#')[0];
		// console.log('landingpage: ' + window.entryUrl);
	}

	if(to.meta.login){
		const token = Vue.prototype.$util.getLocalStorage('authToken');

		if(!token){
			Vue.prototype.$util.setSessionStorage('LoginRedirect', location.href);
			RouterModel.push({name: 'login'});
		}
	}
		
	document.title = to.meta.title || "锦致生活社区团购";
	next();
})

RouterModel.afterEach((to, from) => {
})

export default RouterModel;