const tab_user = () => import('@/views/user/tabbar-user');
const UserTeam = () => import('@/views/user/module-team/');
const TeamInvite = () => import('@/views/user/module-team/invite')
const TeamMembers = () => import('@/views/user/module-member/');
// const UserInvitation = () =>
//   import('@/views/user/module-invitation/');
const UserServer = () => import('@/views/user/module-server/');

const UserInformation = () => import('@/views/user/user-information-set/');

const UserOrderEntityList = () => import('@/views/user/order-entity-list/');
const ShopOrderList = () => import('@/views/user/shop-order-list/');
const ShopWorkList = () => import('@/views/user/shop-work-list/');
const UserOrderEleList = () => import('@/views/user/order-ele-list/');
const UserCouponList = () => import('@/views/user/coupon-list/');
// const UserRefundList = () =>
//   import('@/views/user/refund-list/');
const UserPickRecord = () => import('@/views/user/module-balance/pick-record');
const UserCommissionList = () => import('@/views/user/module-commission/list');
const UserCommissionSum = () => import('@/views/user/module-commission/sum');

const UserGoodRemind = () => import('@/views/user/module-goods-remind');

const ShopStatistic = () => import('@/views/user/shop-statistic/');
const ShopRanking = () => import('@/views/user/module-ranking/');
const AddWorkOrder = () => import('@/views/user/shop-order-list/add-work-order');
const WorkOrder = () => import('@/views/user/shop-order-list/work-order');
const RegShop = () => import('@/views/user/reg-shop/');
const SupplyList = () => import('@/views/user/module-supply/');
const PickupList = () => import('@/views/user/module-pickup/');
const BankCard = () => import('@/views/user/module-bank/');
const AddBankCard = () => import('@/views/user/module-bank/add-card');
const BuyVip = () => import('@/views/user/module-vip/');
const BalanceRecord = () => import('@/views/user/module-balance/balance-record');
const JiFenList = () => import('@/views/user/module-jifen/');
const JiFenConvert = () => import('@/views/user/module-jifenconvert/');
const Tabbar = () => import('@/vue/components/Tabbar/');
const UserCouponExchange = () => import('@/views/user/module_coupon_exchange');
//清分
const Clean = () => import('@/views/user/clean');
const Khname = () => import('@/views/user/clean/kh-name');
const XiaoLu = () => import('@/views/home/tabbar-home-xiaolu');

export default [{
  path: "/user",
  name: "user",
  meta: {
    keepAlive: true,
    login: true,
    title: "会员中心"
  },
  components: {
    default: tab_user,
    tabbar: Tabbar
  }
}, {
  path: "/user/team",
  name: "team",
  component: UserTeam,
  meta: {
    title: '合作团长'
  }
}, {
  path: "/user/member",
  name: "members",
  component: TeamMembers,
  meta: {
    title: '旗下会员'
  }
}, {
  path: "/user/remind",
  name: "goodsremind",
  component: UserGoodRemind,
  meta: {
    title: '到货提醒'
  }
}, {
  path: "/user/server",
  name: "user-server",
  component: UserServer
}, {
  path: "/user/information",
  name: "user-information",
  meta: {
    login: true,
    title: '我的资料'
  },
  component: UserInformation
}, {
  path: "/user/order/list/:active",
  name: "user-order-list",
  props: true,
  components: {
    default: UserOrderEntityList,
    tabbar: Tabbar
  },
  meta: {
    title: '订单中心'
  }
}, {
  path: "/user/orderEle/list/:active",
  name: "user-order-ele-list",
  props: true,
  component: UserOrderEleList,
  meta: {
    title: "我的优惠券"
  }
}, {
  path: "/user/coupon/list",
  name: "user-coupon-list",
  props: true,
  component: UserCouponList,
  meta: {
    title: "优惠券"
  }
}, {
  path: "/user/module-balance/pick-record",
  name: "user-pick-record",
  component: UserPickRecord,
  meta: {
    title: "提现记录"
  }
}, {
  path: "/user/shop-order-list/:teamId",
  name: "shop-order-list",
  component: ShopOrderList,
  props: true,
  meta: {
    title: "门店订单"
  }
}, {
  path: "/user/shop-work-list/:teamId",
  name: "shop-work-list",
  component: ShopWorkList,
  props: true,
  meta: {
    title: "售后工单"
  }
}, {
  path: "/user/commission/list",
  name: "commission-list",
  component: UserCommissionList,
  meta: {
    title: "佣金明细"
  }
}, {
  path: "/user/commission/sum",
  name: "commission-sum",
  component: UserCommissionSum,
  meta: {
    title: "提成统计"
  }
}, {
  path: "/user/shop-statistic",
  name: "shop-statistic",
  component: ShopStatistic,
  meta: {
    title: "门店统计"
  }
}, {
  path: "/user/shop-ranking",
  name: "shop-ranking",
  component: ShopRanking,
  meta: {
    title: "门店排行"
  }
}, {
  path: "/user/add-work-order",
  name: "add-work-order",
  component: AddWorkOrder,
  props: (route) => (route.query),
  meta: {
    title: "申请售后"
  }
}, {
  path: "/user/work-order/:workId",
  name: "work-order",
  component: WorkOrder,
  props: true,
  meta: {
    title: "售后详情"
  }
}, {
  path: "/user/shop-reg",
  name: "reg-shop",
  component: RegShop,
  props: (route) => (route.query),
  meta: {
    title: "注册团长",
    login: true
  }
}, {
  path: "/user/invite",
  name: "team-invite",
  component: TeamInvite,
  meta: {
    title: "邀请注册团长",
  }
}, {
  path: "/user/supply",
  name: "supply-info",
  component: SupplyList,
  meta: {
    title: "供应商中心",
  }
}, {
  path: "/user/pickup",
  name: "pickup-list",
  component: PickupList,
  meta: {
    title: "送货清单",
  }
}, {
  path: "/user/bank",
  name: "bank-card",
  component: BankCard,
  meta: {
    title: "我的银行卡",
    login: true
  }
}, {
  path: "/user/bank/add",
  name: "add-card",
  component: AddBankCard,
  meta: {
    title: "添加银行卡",
    login: true
  }
}, {
  path: "/user/vip",
  name: "buy-vip",
  component: BuyVip,
  props: (route) => (route.query),
  meta: {
    title: "升级VIP会员",
    login: true
  }
}, {
  path: "/user/balance/list",
  name: "balance-list",
  component: BalanceRecord,
  meta: {
    title: "余额列表",
    login: true
  }
},
{
  path: "/user/coupon/exchange",
  name: "coupon-exchange",
  component: UserCouponExchange,
  meta: {
    title: "优惠券兑换",
    login: true
  }
}, {
  //清分
  path: "/user/clean",
  name: "clean",
  component: Clean,
  meta: {
    //记录位置
    keepAlive: true,
    title: "清分",
    login: true
  }
}, {
  //清分 跳转开户行名称界面
  path: "/user/khname",
  name: "khname",
  component: Khname,
  meta: {
    title: "开户行名称",
    login: true
  }
}, {
  //积分明细
  path: "/user/jifen/list",
  name: "jifen-list",
  component: JiFenList,
  meta: {
    title: "我的积分",
    login: true
  }
}, {
  path: "/user/jifenconvert/integraCouponlList",
  name: "jifen-convert",
  component: JiFenConvert,
  meta: {
    title: "积分兑换",
    login: true
  }
}, {
  path: "/xiaolu",
  name: "xiaolu",
  components:{default: XiaoLu, tabbar: Tabbar },
  meta: {
    title: "小鹿集市",
    login: true
  }
}
]