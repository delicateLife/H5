const tab_class = () => import('@/views/items/tabbar-class.vue');
const ItemSearch = () => import('@/views/items/search/');
const ItemSearchResult = () => import('@/views/items/search-result/');
const ItemList = () => import('@/views/items/list/');
const ItemDetail = () => import('@/views/items/detail/');
const ItemSubject = () => import('@/views/items/subject/');

const Tabbar = () => import('@/vue/components/Tabbar/');


export default [{
  path: "/items",
  name: "class",
  meta: {
    keepAlive: true,
    title: '商品分类'
  },
  components: {
    default: tab_class,
    tabbar: Tabbar
  }
}, {
  path: "/items/search",
  name: "search",
  meta: {
    keepAlive: true,
    title: '商品搜索'
  },
  component: ItemSearch
}, {
  path: "/items/search/result",
  name: "search-result",
  meta: {
    keepAlive: true,
    title: '商品搜索'
  },
  components: {
    default: ItemSearchResult,
    tabbar: Tabbar
  },

  props: {
    default: (route) => (route.query)
  }
}, {
  path: "/items/detail/:itemId",
  name: "detail",
  props: true,
  component: ItemDetail,
  meta: {
    title: '商品详情'
  }
}, {
  path: "/items/subject/:subjectId",
  name: "subject",
  props: true,
  component: ItemSubject
}, {
  path: "/items/list",
  name: "list",
  meta: {
    // keepAlive: true
    title: "商品分类"
  },
  components: {
    default: ItemList,
    tabbar: Tabbar
  },

  props: {
    default: (route) => route.query
  }
}]