const login = () => import('@/views/login/login');
const registerInvite = () => import('@/views/login/register-invite/')
const forbidden = () => import('@/views/login/forbidden/')

export default [{
  path: "/login",
  name: "login",
  component: login,
  meta: {
    title: "授权登录"
  }
}, {
  path: "/register/invite",
  name: "register-invite",
  component: registerInvite,
  props: (route) => (route.query),
  meta: {
    login: true,
    title: "受邀注册"
  }
}, {
	path: "/login/forbidden",
	name: "login-forbidden",
	component: forbidden,
	meta: {
	  title: "账户禁用"
	}
}]