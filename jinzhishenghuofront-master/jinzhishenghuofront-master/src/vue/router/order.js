const tab_cart = () => import(/* webpackChunkName: "tabbar-cart" */ '@/views/order/tabbar-cart.vue');
const PlaceOrderEntity = () => import(/* webpackChunkName: "placerder-entity" */ '@/views/order/place-order-entity/');
const PlaceOrderVirtual = () => import(/* webpackChunkName: "placeOrder-virtual" */ '@/views/order/place-order-virtual/');
const Payment = () => import(/* webpackChunkName: "payment" */ '@/views/order/payment/');
const PaymentStatus = () => import(/* webpackChunkName: "payment-status" */ '@/views/order/payment-status/');

const Tabbar = () => import(/* webpackChunkName: "Tabbar" */ '@/vue/components/Tabbar/');

const Gift = ()=>import('@/views/order/gift');

export default [{
			path: "/order",
			name: "cart",
			components: {default: tab_cart, tabbar: Tabbar },
			meta: {
				keepAlive: true,
				title: '我的购物车',
				login: true
			}
	},{
			path: "/order/placeOrderEntity",
			name: "placeOrderEntity",
			component: PlaceOrderEntity,
			meta: {
				title: '生成订单',
				login: true
			}
	},{
			path: "/order/placeOrderVirtual",
			name: "placeOrderVirtual",
			component: PlaceOrderVirtual
	},{
			path: "/order/payment/:payId",
			name: "payment",
			component: Payment,
			props: true,
			meta: {
				title: '订单详情',
				login: true
			}
	},{
			path: "/order/paystatus/:status",
			name: "paymentStatus",
			component: PaymentStatus,
			props: true,
			meta: {
				title: '支付结果',
			}
	},{
		path: "/order/gift",
		name: "gift",
		component: Gift,
		meta: {
			title: '返赠',
			login: true
		}
}]
