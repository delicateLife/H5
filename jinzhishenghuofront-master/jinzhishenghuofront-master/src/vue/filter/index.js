import moment from 'moment';
moment.locale('zh-cn');
import {
  IMAGE_PREFIX
} from '@/api/goods';

export const dateFormat = (value, format = 'YYYY-MM-DD') => {
  if (!value) {
    return '';
  }

  if (typeof (value) === "number") {
    return moment(value * 1000).format(format);
  } else {
    return moment(value).format(format);
  }
}

export const dateTimeFormat = (value, format = 'MM-DD HH:mm') => {
  return dateFormat(value, format);
}

export const yuan = (value, prefix = true) => {
  if (isNaN(value)) {
    return value;
  }

  return prefix ? '¥' + (value).toFixed(2) : (value).toFixed(2);
}

export const image = (value) => {
  if (!value) {
    return value;
  }

  if (value.slice(0, 4) !== "http") {
    value = IMAGE_PREFIX + value;
  }

  return value;
}

export const bankCard = (value) => {
  if (!value) {
    return '';
  }

  let newValue = '**** **** **** ';
  newValue += value.substr(value.length - 4);
  return newValue;
}

export default {
  install(Vue) {
    Vue.filter('yuan', yuan)
    Vue.filter('dateFormat', dateFormat)
    Vue.filter('dateTimeFormat', dateTimeFormat),
    Vue.filter('image', image),
    Vue.filter('bankCard', bankCard)
  },
}