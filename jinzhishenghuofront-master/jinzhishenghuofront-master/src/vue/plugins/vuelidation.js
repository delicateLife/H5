import Vuelidation, { defaultOptions } from 'vuelidation';

defaultOptions.methods = {
	mobile(value){
		let msg = "请填入正确的手机号码";
		let valid = /^1[0-9]{10}$/.test(value);
		return [valid, msg];
	},
	idcard(value) {
		let msg = "请输入正确的身份证号码";
		let valid = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/.test(value);
		return [valid, msg];
	},
	bankcard(value) {
		let msg = "请输入正确的银行卡号";
		let valid = /^\d{16}|\d{19}$/.test(value);
		return [valid, msg];
	}
}

export default Vuelidation