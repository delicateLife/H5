import { GOODS_DETAIL } from "@/api/goods";
import { CART_ADD } from "@/api/shop";

export default {
  data() {
    return {
      skuGoods: null,
      showSku: false,
      cartInfo: "",
    }
  },

  watch: {
    showSku() {
      let tabbar = window.tabbar;
      tabbar && (tabbar.show = !this.showSku);
    }
  },

  methods: {
    //添加到购物车，记件方式
    addCart(goods, skuData, plus = true) {
      if (goods.hasoption && (!this.showSku || goods.id !== this.skuGoods.id)) {
        if (!this.skuGoods || goods.id != this.skuGoods.id) {
          this.$reqGet(GOODS_DETAIL, {
            productId: goods.id,
          }).then(res => {
            const detail = res.data.data;
            this.initSkuData(detail);
            this.skuGoods = goods;
            this.skuGoods.detail = detail;
            this.showSku = true;
          });
        } else {
          this.showSku = true;
        }
      } else {
        const user = this.user;
        let cartItem = {
          mid: user.id,
          teamid: user.upshopTid,
          openid: user.openid,
          productId: goods.id,
          total: plus ? 1 : -1,
          marketprice: (user.isVip && goods.vipprice && goods.isvip) ? goods.vipprice : goods.marketprice,
          vipprice: goods.vipprice
        };

        if (this.showSku && skuData) {
          cartItem.skuId = skuData.selectedSkuComb.id;
          cartItem.total = skuData.selectedNum;
          const sku = this.getGoodsSku(cartItem.skuId, goods.detail);
          cartItem.skuTitle = sku.title;
          cartItem.marketprice = (user.isVip && sku.vipprice && goods.isvip) ? sku.vipprice : sku.marketprice;
          cartItem.vipprice = sku.vipprice;
        }

        if (goods.maxbuy && (goods.cartNum + cartItem.total) > goods.maxbuy) {
          this.$toast('已经超过单次购买数量');
          return;
        }
        this.$reqPost(CART_ADD, cartItem, {
          hideLoading: true
        }).then(res => {
          this.$toast({
            message: "已加入到购物车",
          });
          this.cartInfo = (this.cartInfo === "") ? cartItem.total : String(parseInt(this.cartInfo) + cartItem.total);
          this.changeCartNum(cartItem.total);
          goods.cartNum += cartItem.total;
          this.showSku = false;
        });
      }
    },
    //这里把商品添加到购物车
    addSkuCart() {
      const skuData = this.$refs.skuDom.getSkuData();

      if (!skuData.selectedSkuComb) {
        this.$toast('请先选择商品规格');
        return;
      }

      this.addCart(this.skuGoods, skuData);
    },
    //应该是立即下单
    doBuyNow(goods) {
      if (!goods.hasoption) {
        this.$util.setSessionStorage("order", {
          cartItems: [{
            productId: goods.id,
            total: 1
          }],
          payback: {
            name: this.$route.name,
            params: this.$route.params
          }
        });
        this.toPay();
      } else if (this.showSku) {
        const skuData = this.$refs.skuDom.getSkuData();

        if (!skuData.selectedSkuComb) {
            this.$toast('请先选择商品规格');
            return;
        }

        const sku = this.getGoodsSku(skuData.selectedSkuComb.id, goods);

        this.$util.setSessionStorage("order", {
          cartItems: [{
            productId: goods.id,
            skuId: sku.id,
            total: skuData.selectedNum,
            skuTitle: sku.title,
            marketprice: sku.marketprice,
            vipprice: sku.vipprice
          }],
          payback: {
            name: this.$route.name,
            params: this.$route.params
          }
        });
        this.toPay();
      } else {
        this.showSku = true;
      }
    },

    toPay() {
      this.$router.push({
        name: "placeOrderEntity"
      });
    },

    getGoodsSku(skuId, goods) {
      for (let i = 0; i < goods.skus.length; ++i) {
        const sku = goods.skus[i];

        if (sku.id === skuId) {
          return sku;
        }
      }
    },
  }
}
