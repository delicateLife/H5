import { IMAGE_PREFIX } from "@/api/goods";

export default {
	props: {
		goods: {
			type: Object,
			default: () => ({})
		},
	},
	
	computed: {
		goodsStatusToMe(){
			const is_buy = this.goods.is_buy
			const is_collect = this.goods.is_collect
			return is_buy ? '我购买过' : is_collect ? '我收藏过' : ''
		},

		pickupTime() {
			let pickTime = this.goods.tihuoday;
			let isContinuity=this.goods.isContinuity;
			//连续卖的商品
			if(isContinuity){
				 let jin=new Date();
				 let year = jin.getFullYear();
         let month = jin.getMonth() + 1;
         let strDate = jin.getDate();
				 let hour=jin.getHours();
				 if (month >= 1 && month <= 9) {
            month = "0" + month;
         }
         if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
         }
				 let day=year +"-"+ month +"-"+ strDate+" 12:00:00";
				 if(jin.getHours()>19){
					 return Date.parse(day.replace(/-/g, '/'))/1000+2*24*60*60
				 }else{
					 return Date.parse(day.replace(/-/g, '/'))/1000+24*60*60
				 }
			}
			
			if (!pickTime) {
				if (this.globalConfig) {
					pickTime = this.globalConfig.expectedPickTime;
				}
			}
	
			return Date.parse(this.goods.timeend.replace(/-/g, '/')) / 1000 + pickTime * 60;
		  }
	},

	methods:{
		OnClick(){
			this.$emit("doclick")
		},

		convertImageUrl(url) {
			if (!url) {
				return url;
			}

			if (url.slice(0, 4) !== "http") {
              url = IMAGE_PREFIX + url;
 	         }

  	        return url;
		}
	},
}