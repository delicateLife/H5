export default {
  data() {
    return {
      showSku: false,
      skuData: null,
    };
  },

  methods: {
    initSkuData(goods) {

      if ((goods.skus.length === 0) || (goods.productSpecList.length === 0)) {
        return;
      }

      this.skuData = {
        goods: {
          title: goods.title,
          picture: goods.thumbSquare || goods.thumb
        },
        sku: {
          tree: [],
          list: [],
          price: goods.marketprice,
          vipprice: goods.vipprice,
          unit: goods.unit,//商品的单位
          stock_num: goods.total - goods.salesreal,
          hide_stock: false,
          none_sku: false,
          collection_id: goods.id
        }
      };

      let skuTree = this.skuData.sku.tree;

      goods.productSpecList.forEach((spec, i) => {
        skuTree[i] = {
          k: spec.title,
          k_id: spec.id,
          k_s: 's' + (i + 1),
          v: []
        }

        let skuTreeValue = skuTree[i].v;

        spec.specItemList.forEach((item, j) => {
          skuTreeValue[j] = {
            id: item.id,
            name: item.title,
            imgUrl: item.thumb
          }
        })
      });

      let skuList = this.skuData.sku.list;

      goods.skus.forEach((sku, i) => {
        skuList[i] = {
          id: sku.id,
          price: sku.marketprice * 100,
          vipprice: sku.vipprice,
          stock_num: sku.stock - sku.salesreal,
        };

        let specs = sku.specs.split('_');
        specs.forEach((spec, j) => {
          const name = 's' + (j + 1);
          skuList[i][name] = spec;
        })
      })
    },
  }
}