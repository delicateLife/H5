import wx from 'weixin-js-sdk';
import { WXJS_CONFIG } from "@/api/global";

const isAndroid = navigator.userAgent.indexOf('Android') > -1 || navigator.userAgent.indexOf('Adr') > -1;
let configUrls = [];
let lastShareUrl = '';

export default {
  data() {
    return {
      wx
    }
  },

  methods: {
    wxconfig() {
      let vm = this;
      this.getConfigUrl().then(url => {
        const find = configUrls.findIndex((value, index, arr) => {
          return value === url;
        });

        if (find > -1) {
          console.log('find wx config url: ' + url);
          return;
        }

        // 进行签名的时候
        vm.$reqGet(WXJS_CONFIG, { url }, {hideLoading: true}).then(res => {
          configUrls.push(url);
          let config = JSON.parse(res.data.data);
          // config.debug = true;
          //js sdk 1.4.0新引入的，后台还没有支持
          // config.jsApiList.push('updateAppMessageShareData');
          // config.jsApiList.push('updateTimelineShareData');
          wx.config(config);
          // wx.error(function (err) {
          //   vm.$dialog.alert({
          //     message: '微信JsSDK配置错误：' + err.errMsg + ', ' + url
          //   });
          // });
        })
      });
    },

    getConfigUrl() {
      if (!window.entryUrl) {
        window.entryUrl = location.href.split('#')[0];
      }

      let vm = this;
      return new Promise(resolve => {
        vm.isWxApp().then(res => {
          (res || !isAndroid) ? resolve(window.entryUrl) : resolve(location.href.split('#')[0])
        })
      });
    },
    wxShare(title, desc, link, img) {
      if (lastShareUrl == this.$route.fullPath) {
        return;
      }

      lastShareUrl = this.$route.fullPath;
      let vm = this;

      this.isWxApp().then(app => {
        if (app) {
          wx.miniProgram.postMessage({
            data: {
              title:title,
              desc: desc,
              link: link,
              img: img
            }
          });
        } else {
          vm.wx.ready(function () { //需在用户可能点击分享按钮前就先调用
            wx.onMenuShareTimeline({
              title: title,
              desc: desc,
              link: link,
              imgUrl: img,
              success: function () {},
              fail: function (data) {
                vm.$dialog.alert({
                  message: data.errMsg
                });
              }
            });
            vm.wx.onMenuShareAppMessage({
              title: title,
              desc: desc,
              link: link,
              imgUrl: img,
              type: 'link', // 分享类型,music、video或link，不填默认为link
              success: function () {},
              fail: function (data) {
                vm.$dialog.alert({
                  message: data.errMsg
                });
              }
            });
          });
        }
      })
    },

    isWxApp() {
      let vm = this;
      return new Promise(resolve => {
        vm.wx.miniProgram.getEnv(res => {
            if (res.miniprogram) {
              resolve(true);
            } else {
              resolve(false);
            }
        })
      });
    }
  }
}
