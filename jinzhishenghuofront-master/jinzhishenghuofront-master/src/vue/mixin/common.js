import moment from 'moment';

let user = null;
let shop = null;

let mixMethod = {
  data() {
    return {
      user: this.getUser(),
    }
  },

  methods: {
    getUser() {
      if (!user) {
        user = this.$util.getLocalStorage('user');
        // console.log('get user');
      }

      return user;
    },

    getShop() {
      return shop;
    },

    getUri() {
      let uri = window.location.origin;

      if (this.$router.options.base) {
        uri += this.$router.options.base;
      } else {
        uri += '/';
      }

      return uri;
    },

    saveUser(newUser) {
      if (newUser) {
        user = newUser;
      }

      this.$util.setLocalStorage('user', user);
    },

    saveShop(newShop) {
      shop = newShop;
    },

    changeCartNum(num) {
      let tabcart = window.tabbar;
      tabcart && (tabcart.cartNum += num);
    },

    isAssistMode() {
      const assistMode = this.$util.getSessionStorage('assistMode');
      return (assistMode && assistMode > 0);
    },

    setAssistMode(mode) {
      if (mode) {
        this.$util.setSessionStorage('assistMode', 1);
      } else {
        this.$util.removeStorage('assistMode', false);
      }
    },

    //是否需要确认提货点
    showPickPromote() {
      const confirm = this.$util.getLocalStorage('firstLogin') || false;
      if (confirm) {
        return true;
      }

      const lastShopTid = this.$util.getLocalStorage('lastShopTid');

      if (lastShopTid !== this.user.upshopTid) {
        return true;
      }

      return false;
    },
    checkShop() {
      let shop = this.getShop();
      //不是团长或者禁用
      if ((shop.isteam !== 3) || (shop.status === 9)) {
        return -1;
      } else {
        if (shop.sleepTime) {//是否打烊
          const startTime = moment(shop.sleepStartTime).unix();
          const endTime = moment(shop.sleepEndTime).unix();
          const serverTime = moment().unix();
          shop.isSleep = (serverTime >= startTime) && (serverTime <= endTime);
          this.shopSleep = shop.isSleep;
        }

        return (shop.isSleep ? 0 : 1);
      }
    },

  }
}

export default {
  install(Vue) {
    Vue.mixin(mixMethod);
  }
}
