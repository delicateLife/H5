// 滚动条记录， 适用于 keep-alive 组件
export default {
	data(){
		return {
			scrollTop: 0,
			showArrow: false
		}
	},
	
	mounted(){
		const vm = this;
		
		vm.$el.addEventListener('scroll', vm.$util.debounce(function() {
			vm.scrollTop = vm.$el.scrollTop;
			vm.showArrow = (vm.scrollTop > 100);
		}, 50))		
	},
	
	activated(){
		this.$el.scrollTop = this.scrollTop;

	},

	methods: {
		backTop() {
			this.$el.scrollTop = 0;
		}
	}
}