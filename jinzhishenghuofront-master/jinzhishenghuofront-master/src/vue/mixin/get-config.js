import {
  GLOBAL_CONFIG,
  GLOBAL_SHARE
} from "@/api/global";

let loadingConfig = false;
let loadingShare = false;

export default {
  data() {
    return {
      globalConfig: null,
      globalShare: null
    }
  },

  methods: {
    getGlobalConfig(forceRefresh = false) {
      const vm = this;
      return new Promise((resovle, reject) => {
        let config = forceRefresh ? null : vm.$util.getSessionStorage("config");

        if (config !== null) {
          vm.globalConfig = config;
          resovle(config);
        } else if (!loadingConfig) {
          loadingConfig = true;
          vm.$reqGet(GLOBAL_CONFIG, {centerId: this.user.centreId}).then(res => {
            config = res.data.data;
            vm.$util.setSessionStorage("config", config);
            vm.globalConfig = config;
            resovle(config);
            loadingConfig = false;
          }).catch(reject);
        }
      })
    },
    getGlobalShare() {
      const vm = this;
      return new Promise((resovle, reject) => {
        let share = vm.$util.getSessionStorage("share");

        if (share !== null) {
          vm.globalShare = share;
          resovle(share);
        } else if (!loadingShare) {
          loadingShare = true;
          vm.$reqGet(GLOBAL_SHARE, {centerId: this.user.centreId}).then(res => {
            share = res.data.data;
            vm.$util.setSessionStorage("share", share);
            vm.globalShare = share;
            resovle(share);
            loadingShare = false;
          }).catch(reject);
        }
      })
    },
  }
}