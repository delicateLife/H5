import { SHOPINFO } from '@/api/shop';
export default {
	methods: {
		getShopInfo(...keys){
			const vm = this;
			return new Promise((resolve, reject) => {
				const shop_id = window.sessionStorage.getItem('id');
				if(shop_id === null){
					const shop_id = vm.$util.getLocationParam("shop_id");
					vm.$reqGet(SHOPINFO, {shop_id : shop_id}).then(res => {
						const data = res.data.data;
						vm.$util.setSessionStorage(data);
						resolve(data);
					}).catch(reject);
				}else{
					resolve(vm.$util.getSessionStorage(...keys));
				}
			})
		},
	}
}