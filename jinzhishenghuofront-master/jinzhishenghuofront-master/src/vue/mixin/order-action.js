import {
  CANCEL_ORDER,
  CANCEL_GOODS
} from '@/api/order';

export default {
  methods: {
    async cancelOrder(order) {
      if (order.status === 1) {
        let cancelAble = true;

        for (let goodsIndex = 0; goodsIndex < order.orderProducts.length; ++goodsIndex) {
          const goods = order.orderProducts[goodsIndex];
          if (goods.uncancelable === 1) {
            cancelAble = false;
            break;
          }
        };

        if (!cancelAble) {
          this.$dialog.alert({
            message: '有不支持无理由退单商品，无法取消订单'
          });
          return;
        }
      }

      this.$dialog.confirm({
        message: "确定要取消该订单吗?"
      }).then(() => {
        this.$reqPost(CANCEL_ORDER, {
          orderId: order.id
        }).then(res => {
          order.status = -1;
          order.orderProducts.forEach(goods => {
            goods.status = -1;
          });
          this.$toast("已取消该订单");
        });
      }).catch(() => {});
    },

    canCancelGoods(order, goods) {

      return (order.orderProducts.length > 1) 
        && (this.user.id === order.mid || this.user.id === order.tMid) 
        && (goods.uncancelable !== 1) && (goods.status === 1);
    },

    async cancelGoods(goods) {
      this.$dialog.confirm({
        message: "确定要取消商品吗?"
      }).then(() => {
        this.$reqPost(CANCEL_GOODS, {
          goodsId: goods.id
        }).then(res => {
          goods.status = -1;
          this.$toast("已取消该商品");
        });
      }).catch(() => {});
    },
    async delOrder(order) {
      const id = order.id;
      await this.$dialog.confirm({
        message: "确定要删除该订单吗?"
      });
      this.items.splice(i, 1);
      this.$toast("已删除该订单");
    },
    async receiptOrder(order) {
      const id = order.id;
      await this.$dialog.confirm({
        message: "请确认收到货物, 确认收货后无法撤销!"
      });
      this.items[i].status = 40;
      this.$toast("已确认收货");
    },
    reminderOrder(order) {
      const id = order.id;
      this.items[i].is_can_reminder = false;
      this.$toast("已提醒卖家发货, 请耐心等待哦~");
    },
  }
}